<?php
/**
 * Created by PhpStorm.
 * User: luke
 * Date: 03/11/14
 * Time: 11:17AM
 */

function getAllowedExtensions () {
    $extensionsFile = BASE_PATH . '/extensions.smw';
    $extensions = array('html');
    if (is_file($extensionsFile)) {
        $extensionsFileContent = file_get_contents($extensionsFile);
        if ($extensionsFileContent !== '') {
            $extensions = explode(',', $extensionsFileContent);
        }
    }

    return $extensions;
}

function addAllowedExtension ($newExtensions) {
    $extensionsFile = BASE_PATH . '/extensions.smw';

    $currentExtensions = getAllowedExtensions();

    foreach ($newExtensions as $newExtension) {
        if (array_search($newExtension, $currentExtensions) === FALSE) {
            $currentExtensions[] = $newExtension;
        }
    }

    file_put_contents($extensionsFile, implode(',', $currentExtensions), LOCK_EX);

    return getAllowedExtensions();
}

function removeAllowedExtension ($extension) {
    $extensionsFile = BASE_PATH . '/extensions.smw';

    $currentExtensions = getAllowedExtensions();

    $index = 0;

    foreach ($currentExtensions as $currentExtension) {
        if ($currentExtension === $extension) {
            break;
        }
        $index++;
    }

    unset($currentExtensions[$index]);

    echo $currentExtensions[$index];

    $currentExtensions = array_values($currentExtensions);

    file_put_contents($extensionsFile, implode(',', $currentExtensions), LOCK_EX);

    return getAllowedExtensions();
}

function allowedExtension ($extension) {
    if (substr($extension, 0, 1) === '.') {
        $extension = substr($extension, 1);
    }

    $extensions = getAllowedExtensions();

    if (array_search($extension, $extensions) !== FALSE) {
        return true;
    }

    return false;
}

function fileAccessAllowed ($file) {
    if (strpos($file, '.') !== FALSE) {
        $extension = explode('.', $file);

        $extension = array_pop($extension);

        if (substr($extension, 0, 1) !== '/') {
            return allowedExtension($extension);
        }
    }

    return false;
}