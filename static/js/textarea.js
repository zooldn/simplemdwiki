(function () {
    var $document = document.querySelector('#document'),
        PRELENGTH = 0;

    $document.addEventListener('keydown', function (e) {
        var keyCode = e.keyCode || e.which;

        var start = $document.selectionStart;
        var end = $document.selectionEnd;

        if (e.shiftKey && keyCode === 9) {
            e.preventDefault();
        } else if (keyCode === 9) {
            e.preventDefault();

            $document.value = $document.value.substring(0, start) + "\t" + $document.value.substring(end);

            $document.selectionStart = $document.selectionEnd = start + 1;
        }/* else if (keyCode === 13) {
            var lineByLine = $document.value.split("\n");
            var currentCharacters = 0;
            var previousCharacters = 0;
            var line;

            for (var i = 0, ii = lineByLine.length; i < ii; i += 1) {
                currentCharacters += lineByLine[i].length;

                if (start < currentCharacters && start > previousCharacters) {
                    line = i;
                    break;
                }
            }

            var previousLine = line - 2;

            if (!lineByLine[previousLine]) {
                previousLine = lineByLine.length - 1;
            }

            var preStr = lineByLine[previousLine].match(/\s+/);

            if (preStr && preStr.index === 0) {
                preStr = preStr[0];
                PRELENGTH = preStr.length;

                $document.value = $document.value.substr(0, start) + preStr + $document.value.substring(end);
                $document.selectionStart = $document.selectionEnd = start;
            }
        }*/
    }, false);

    $document.addEventListener('keyup', function (e) {
        /*var keyCode = e.keyCode || e.which;

        if (keyCode === 13) {
            $document.selectionStart = $document.selectionEnd = $document.selectionStart + PRELENGTH;
        }*/
    }, false);
})();