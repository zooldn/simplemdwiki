(function () {
    var $document = document.querySelector('#document'),
        $visibility = document.querySelector('#privacy'),
        $submit = document.querySelector('.submit');

    $submit.addEventListener('click', function (e) {
        e.preventDefault();
        if ($document.value.length > 0) {
            var url = API + PATH;

            reqwest({
                url: url,
                method: 'post',
                data: JSON.stringify({'page': $document.value, 'privacy': $visibility.value}),
                success: function (resp) {
                    console.log(resp);
                    if (resp.success) {
                        window.location.href = url;
                    }
                },
                error: function (err) {
                    console.error(err);
                }
            });
        }
    }, false);
})();