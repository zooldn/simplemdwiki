<?php

session_start();

define('BASE_PATH', realpath(dirname(__FILE__)));

require 'vendor/autoload.php';

require 'config.php';

require 'libs/setup.php';

define('PAGES_PATH', 'pages/');

$app = new \Slim\Slim();

$app->config(array(
    'templates.path' => 'templates'
));

function fileExists($path) {
    if (is_file('./' . PAGES_PATH . $path)) {
        return true;
    } else {
        return false;
    }
}

function get_diff($old, $new){
    if (is_string($old)) {
        $old = explode("\n", $old);
    }
    if (is_string($new)) {
        $new = explode("\n", $new);
    }
    $matrix = array();
    $maxlen = 0;
    foreach($old as $oindex => $ovalue){
        $nkeys = array_keys($new, $ovalue);
        foreach($nkeys as $nindex){
            $matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
                $matrix[$oindex - 1][$nindex - 1] + 1 : 1;
            if($matrix[$oindex][$nindex] > $maxlen){
                $maxlen = $matrix[$oindex][$nindex];
                $omax = $oindex + 1 - $maxlen;
                $nmax = $nindex + 1 - $maxlen;
            }
        }
    }
    if($maxlen == 0) return array(array('old'=>$old, 'new'=>$new));
    return array_merge(
        get_diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
        array_slice($new, $nmax, $maxlen),
        get_diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
}

function updateLog($path, $diff) {
    $logFile = str_replace('.html', '.log', $path);

    $log = "[" . date('c') . "] " . $_SESSION['user']['name'];

    if (is_file($logFile)) {
        $diff = $diff[count($diff) - 1];
        $old = '';
        $new = '';

        if (count($diff['old']) > 0) {

            foreach ($diff['old'] as $olden) {
                $old .= '[-]' . $olden . "\n";
            }

        }


        if (count($diff['new']) > 0) {

            foreach ($diff['new'] as $newen) {
                $new .= '[+]' . $newen . "\n";
            }

        }


        $log .= " EDITED";
        if ($old !== '' || $new !== '') {
            $log .= "\n";
            $log .= "DIFF---------------\n";
            $log .= $old;
            $log .= $new;
            $log .= "------------ENDDIFF";
        }
        $current = file_get_contents($logFile);
        file_put_contents($logFile, $log . "\n" . $current, LOCK_EX);
    } else {
        $log .= " CREATED";
        file_put_contents($logFile, $log, LOCK_EX);
    }
}

function getFile($path) {
    $path = explode('/', PAGES_PATH . $path);
    $current = '.';
    $file = '';

    foreach ($path as $dir) {
        $current = $current . '/' . $dir;
        $file = $file . '/' . $dir;

        if (strpos($file, '.') !== FALSE) {
            if (fileAccessAllowed($current)){
                if (is_file($current)) {
                    $content = file_get_contents($current);
                }
                break;
            } else {
                if (!is_dir($current)) {
                    break;
                }
            }
        }
    }

    if (isset($content)) {
        return $content;
    } else {
        return false;
    }
}

function getFilePermission($path) {
    $permissionFile = str_replace('.html', '.perm', $path);

    if (is_file($permissionFile)) {
        $content = file_get_contents($permissionFile);
        $content = explode("\n", $content);
        $perm = $content[0];

        return $perm;
    } else {
        return 'public';
    }
}

function createFile($path, $content, $privacy) {
    $path = explode('/', PAGES_PATH . $path);
    $current = '.';
    $file = '';

    foreach ($path as $dir) {
        $current = $current . '/' . $dir;
        $file = $file . '/' . $dir;

        if (strpos($current, '.html')) {
            file_put_contents($current, $content, LOCK_EX);
            $permissionFile = str_replace('.html', '.perm', $current);
            file_put_contents($permissionFile, $privacy, LOCK_EX);
            updateLog($current, false);
            break;
        } else {
            if (!is_dir($current)) {
                mkdir($current);
            }
        }
    }

    if (is_file($current)) {
        return $file;
    } else {
        return false;
    }
}

function updateFile($path, $content, $privacy) {
    $path = './' . PAGES_PATH . '/' . $path;

    $old = file_get_contents($path);

    if (file_put_contents($path, $content, LOCK_EX)) {
        $permissionFile = str_replace('.html', '.perm', $path);
        file_put_contents($permissionFile, $privacy, LOCK_EX);
        updateLog($path, get_diff($old, $content));
        return true;
    } else {
        return false;
    }
}

function CheckAuth() {
    $app = \Slim\Slim::getInstance();

    $path = $app->request->getPathInfo();

    if ($path === '/') {
        $path .= 'home.html';
    }

    if ($path) {
        $perm = getFilePermission(PAGES_PATH . $path);
        $GLOBALS['pageVis'] = $perm;
        if ($perm !== 'public') {
            if (!isset($_SESSION['user'])) {
                error_log('Unauthorized: No user set');
                $app->redirect(BASE_URL . 'login');
            }
        }

        if (isset($_SESSION['user'])) {
            if (in_array('create_edit', $_SESSION['user']['permissions'])) {
                $_SESSION['user']['create_edit'] = true;
            } else {
                $_SESSION['user']['create_edit'] = false;
            }
        }
    } else {
        echo 'Error: path wrong';
        exit;
    }
}

function renderStaticFile ($path, $file) {
    $extension = mime_content_type($path);
    header('Content-Type: ' . $extension . '; charset=utf-8');
    echo $file;
}

$app->get('/login', function () use ($app) {
    $app->render('login.php');
});

$app->post('/login', function () use ($app, $users) {
    $data = $app->request->post();

    if (isset($data['username']) && isset($data['password'])) {
        foreach ($users as $user) {
            if ($data['username'] === $user['name'] && $data['password'] === $user['password']) {
                $_SESSION['user'] = $user;
                break;
            }
        }

        $app->redirect(BASE_URL);
    }
});

$app->get('/logout', function () use ($app) {
   session_destroy();
    $app->redirect(BASE_URL);
});

$app->post('/(:path+)', 'CheckAuth', function ($path) use ($app) {
    if (is_array($path)) {
        $path = implode('/', $path);
    }

    try {
        $file = json_decode($app->request->getBody(), true);
        $documentContent = $file['page'];
        $privacy = $file['privacy'];
        if (isset($file['action'])) {
            $action = $file['action'];
        }
    } catch (Exception $e) {
        echo '{"error": "' . $e->getMessage() . '"}';
        exit;
    }

    header('Content-Type: application/json; charset=utf-8');

    if (strpos($path, '.html') !== false) {
        if (fileExists($path) && isset($action) && $action === 'update') {
            if (updateFile($path, $documentContent, $privacy)) {
                echo '{"success": true}';
                exit;
            }
        } else {
            if (createFile($path, $documentContent, $privacy)) {
                echo '{"success": true}';
                exit;
            }
        }
    }

    echo '{"error": "failed"}';
});

// GET route
$app->get('/(:path+)', 'CheckAuth', function ($path = 'home.html') use ($app) {
    $action = $app->request->get('action');
    if (is_array($path)) {
        $path = implode('/', $path);
    }

    $file = getFile($path);
    if ($action && $action === 'edit' && $file) {
        $app->render('edit.php', array('path' => $path, 'visibility' => $GLOBALS['pageVis'], 'content' => $file));
    } else if ($file) {
        $markdown = new Parsedown();
        $app->render('found.php', array('path' => $path, 'content' => $markdown->text($file)));
    } else {
        if ($action && $action === 'create') {
            $app->render('create.php', array('path' => $path));
        } else {
            if (strpos($path, '.html') === false) {
                renderStaticFile($path, $file);
                exit;
            } else {
                $app->render('notfound.php', array('path' => $path));
            }
        }
    }
});

$app->run();
