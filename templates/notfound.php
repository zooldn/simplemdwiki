<!DOCTYPE html>
<html>
    <head>
        <title>Create <?php echo $path; ?></title>
        <link rel="stylesheet" href="static/css/style.css" />
        <script>
            var PATH = '<?php echo $path; ?>';
        </script>
    </head>
    <body>
        <h1>Not Found: <?php echo $path; ?></h1>
        <?php if (isset($_SESSION['user']) && $_SESSION['user']['create_edit']) : ?>
            <p><a href="?action=create" class="create">Create</a></p>
        <?php endif; ?>
    </body>
</html>