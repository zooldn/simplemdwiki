<!DOCTYPE>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $path; ?></title>
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/css/style.css" />
    </head>
    <body>
    <?php if (isset($_SESSION['user'])) : ?>
        <a href="<?php echo BASE_URL; ?>logout" style="float: right;" class="btn">Logout</a>
    <?php else: ?>
        <?php if (SHOW_LOGIN) : ?>
            <a href="<?php echo BASE_URL; ?>login" style="float: right;" class="btn">Login</a>
        <?php endif ;?>
    <?php endif;?>