<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo BASE_URL; ?>" />
    <title>Edit <?php echo $path; ?></title>
    <link rel="stylesheet" href="static/css/style.css" />
    <script>
        var API = '<?php echo BASE_URL; ?>',
            PATH = '<?php echo $path; ?>';
    </script>
</head>
<body>
<?php if (!$_SESSION['user']['create_edit']) : ?>
<h1>No Permissions</h1>
<?php else: ?>
<div class="cheatSheet">
    <div class="show">MD Cheat Sheet</div>
            <pre>
HEADINGS
----------------
# H1
## H2
### H3
#### H4
##### H5
###### H6

EMPHASIS
----------------
Italics: *asterisks* or _underscores_.

Bold: **asterisks** or __underscores__.

Strikethrough: ~~Scratch this.~~

LISTS
----------------
1. First ordered list item
2. Another item
    * Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
    1. Ordered sub-list
4. And another item.

    You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

    To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
    Note that this line is separate, but within the same paragraph.⋅⋅
    (This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses

[I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[I'm a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself]

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com

Here's our logo (hover to see the title text):

Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"

Inline `code` has `back-ticks around` it.

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
s = "Python syntax highlighting"
print s
```

```
No language indicated, so no syntax highlighting.
But let's throw in a <b>tag</b>.
```

Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

The outer pipes (|) are optional, and you don't need to make the raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.

Three or more...

---

Hyphens
            </pre>
</div>
<h1>Edit: <?php echo $path; ?></h1>
<a href="<?php echo BASE_URL . $path; ?>" class="btn">View</a>

<select id="privacy">
    <option value="public"<?php if ($visibility === 'public'):?> selected="selected"<?php endif; ?>>public</option>
    <option value="private"<?php if ($visibility === 'private'):?> selected="selected"<?php endif; ?>>private</option>
</select>
<textarea rows="30" id="document"><?php echo $content; ?></textarea>
<button class="submit btn">Save</button>

<script src="static/js/lib/reqwest.js"></script>
<script src="static/js/textarea.js"></script>
<script src="static/js/edit.js"></script>
<?php endif;?>
</body>
</html>